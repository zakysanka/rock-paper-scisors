import numpy as np
import pandas as pd

from bot import rps_bot


def load_df(file_name: str) -> pd.DataFrame:
    return pd.read_csv(str(file_name), keep_default_na=False, encoding="utf-8-sig")


def ask_yn_question(question: str) -> bool:
    response = input(f"{question} [y/n]: ")
    if (response.lower() == 'y') or (response == ''):
        return True
    else:
        print('Something other than: "y" was returned.\n')
        return False


def game():
    CURRENT_BOT.play()
    if ask_yn_question('Want to play rock, paper, scissors again? '):
        game()
    else:
        CURRENT_BOT.get_total_score()



def get_session_id() -> str:
    df_history = load_df('history.csv')
    if np.isnan(df_history[['session_id']].max()[0]):
        return "1"
    else:
        return str(int(df_history[['session_id']].max()[0]) + 1)


if __name__ == "__main__":
    # try:
    CURRENT_BOT = rps_bot(get_session_id())
    if ask_yn_question('Want to play the game of rock, paper, scissors? '):
        CURRENT_BOT.give_rules()
        game()

    # except:
    #     print('Something went wrong.')

    print('Thank you.')
