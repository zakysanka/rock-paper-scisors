import random


def get_response() -> int:
    return random.randrange(1, 4, 1)
