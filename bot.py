import datetime
import json
import sys
from typing import Tuple

from strategies.random_strategy import get_response


def convert_keys_to_int(input_dict: dict) -> dict:
    try:
        return {int(k): v for k, v in input_dict.items()}
    except ValueError:
        print('IDs in the rules were not all integers.')
        sys.exit()


class rps_bot:
    def __init__(self, play_id: str):
        self.play_id = play_id
        self.total_score = {"bot": 0,
                            "player": 0,
                            "tie": 0,
                            "total": 0}

        with open("config/rules.json", "r", encoding="utf8") as rules_file:
            self.rules = convert_keys_to_int(json.load(rules_file))

    def give_rules(self):
        total = """Rules of the game are following:
You can play:

"""

        for key in self.rules.keys():
            accepts = '", "'.join(self.rules[key]['accept'])
            total = total + f'{key}) {self.rules[key]["name"]} \nfor which input can be: "{accepts}"\n'
            total = total + f'{self.rules[key]["name"]} destroy {self.rules[self.rules[key]["kills"]]["name"]}\n'
            total = total + f'{self.rules[self.rules[key]["dies"]]["name"]} destroy {self.rules[key]["name"]}\n\n'
        print(total)

    def verify_response(self, response: str) -> int:
        for key in self.rules.keys():
            if response in self.rules[key]['accept']:
                return key

        return -1

    def request_response(self) -> int:
        while True:
            response = input(f"What is your play?\n")
            if response == 'help':
                self.give_rules()
            elif (response == 'q') or (response == 'quit'):
                sys.exit()
            else:
                player_play = self.verify_response(response)
                if player_play == -1:
                    print(
                        'Response was not recognized.\nFor rules write "help", to quit write "q" or "quit" or insert correct response.')
                else:
                    return player_play

    def get_result(self, bot: int, player) -> Tuple[str,str]:
        if self.rules[bot]['kills'] == player:
            return "bot", "I have won!!!"
        elif self.rules[bot]['dies'] == player:
            return "player", "You have won!!!"
        else:
            return "tie", "Its a tie!!!"

    def get_total_score(self):
        message = f"""Nice play! In total:
you have won: {self.total_score['player']} times
I have won: {self.total_score['bot']} times
and {self.total_score['tie']} times it was a tie.

Your success is: {(self.total_score['player'] / (self.total_score['total'] - self.total_score['tie'])) * 100}%"""

        print(message)

    def save_result(self, bot: int, player: int, result: str):
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        with open("history.csv", "a") as file_object:
            file_object.write(f'"{self.play_id}","{timestamp}","{bot}","{player}","{result}"\n')

    def give_name(self, play: int) -> str:
        return self.rules[play]['name']

    def play(self):
        bot_play = get_response()
        player_play = self.request_response()
        result, message = self.get_result(bot_play, player_play)
        self.save_result(bot_play, player_play, result)

        self.total_score['total'] += 1
        self.total_score[result] += 1

        response = f"You have played: {self.give_name(player_play)}\n" \
                  f"I have played: {self.give_name(bot_play)}\n" \
                  f"{message}\n"
        print(response)
